Feature: Login / Logout
  Scenario: User wants to login
    Given I'm on the homepage
    When I click the login link
    Then I should see the login screen