Feature: Login / Logout
  Scenario: Users wants to logout
    Given I am in a secured part of the homepage
    When I click the logout button
    Then I should be redirected to the unsecured homepage